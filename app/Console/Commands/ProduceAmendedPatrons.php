<?php

namespace App\Console\Commands;

use App\Lib\Producer\GetAmendedCustomersProducer;
use Illuminate\Console\Command;

class ProduceAmendedPatrons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'produce:amended-patrons {connection}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Produce amended patrons queue';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connection = $this->argument('connection');
        $className = "App\Lib\PatronBase\Connections\\$connection";

        if (class_exists($className)) {
            echo 'Getting modified customers' . PHP_EOL;
            $class = new $className();

            $patrons = new GetAmendedCustomersProducer($class);
            $patrons->send();

            echo 'Sending ';
            echo count($patrons->getPayload());
            echo ' Customers';

        }

        echo PHP_EOL;
        echo 'Finish' .PHP_EOL;

    }
}

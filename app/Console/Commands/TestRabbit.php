<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestRabbit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:rabbit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Rabbit';

    protected $api;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $payload = json_encode(
            [
                'Name' => 'Test',
                'Address' => 'A line of an address'
            ]

        );

        // return $payload;
        // $payload = 'hi to direct exchange';

        app('rabbit')->publish(
            $payload,
            'test', [
            'exchange' => [
                'declare' => true,
                'type'    => 'direct',
                'name'    => 'test',
            ],
        ]);
    }
}

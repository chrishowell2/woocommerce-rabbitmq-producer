<?php

namespace App\Console\Commands;

use App\Lib\Producer\NewSalesProducer;
use Illuminate\Console\Command;

class ProduceNewSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'produce:new-sales {connection}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Produce new sales queue for new WC customers';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connection = $this->argument('connection');
        $className = "App\Lib\PatronBase\Connections\\$connection";

        if (class_exists($className)) {
            echo 'Getting new sales' . PHP_EOL;
            $class = new $className();

            $sales = new NewSalesProducer($class);
            $sales->send();

            echo 'Sending ';
            echo count($sales->getPayload());
            echo ' sales';

        }

        echo PHP_EOL;
        echo 'Finish' .PHP_EOL;

    }
}

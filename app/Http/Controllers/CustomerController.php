<?php

namespace App\Http\Controllers;
use Codexshaper\WooCommerce\Facades\Customer;
use Carbon\Carbon;
use App\Lib\Producer\GetCustomersProducer;
use App\Lib\Producer\GetAmendedCustomersProducer;
use App\Lib\PatronBase\Connections\UKBeta;
use Illuminate\Support\Facades\DB;
use Bschmitt\Amqp\Facades\Amqp;

class CustomerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->connection->dataBaseName = '_UKBeta';
        // $this->patrons = $this->handle();
    }

    public function customers()
    {
        $thing = new GetCustomersProducer(new UKBeta);

        $new_thing = $thing->getPayload();

        return dd($new_thing);

    }

    public function amendedCustomers()
    {
        $thing = new GetAmendedCustomersProducer(new UKBeta);

        $new_thing = $thing->getPayload();

        return $new_thing;
    }
}

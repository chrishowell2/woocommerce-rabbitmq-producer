<?php

namespace App\Http\Controllers;
use App\Lib\Producer\NewSalesProducer;
use App\Lib\PatronBase\Connections\UKBeta;
use Carbon\Carbon;
use Bschmitt\Amqp\Facades\Amqp;

class SalesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->connection = $connection;
        // $this->patrons = $this->handle();
    }

    public function orders()
    {

        $thing = new NewSalesProducer(new UKBeta);

        $new_thing = $thing->getPayload();

        return $new_thing;
    }
}

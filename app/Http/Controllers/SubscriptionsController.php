<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Lib\Producer\NewSubscriptionsProducer;
use App\Lib\PatronBase\Connections\UKBeta;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class SubscriptionsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->connection = $connection;
        // $this->patrons = $this->handle();
    }

    public function getSubscriptions()
    {
        $thing = new NewSubscriptionsProducer(new UKBeta);

        $new_thing = $thing->getPayload();

        return $new_thing;
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Client;

class VerifyKey
{
    public function handle($request, Closure $next)
    {
        // Can always add a database to this if ever needed
        // $valid = Client::where('api_key', $request->api_key)->first();

        $client = $request->route('client');

        if (!config()->has("$client.api_keys")) {
            return response()->json(['Error' => 4 ,'ErrorText' => 'Missing Configuration File']);
        }

        $apiKey = $request->header('X-PatronBase-Api-Key');

        if (!$apiKey) {
            return response()->json(['Error' => 3 ,'ErrorText' => 'Invalid Api Key']);
        }

        $apiKeys = config("$client.api_keys");

        $valid = in_array($apiKey, $apiKeys);

        if ($valid) {
            return $next($request);
        } else {
            return response()->json(['Error' => 3 ,'ErrorText' => 'Invalid Api Key']);
        }

    }
}

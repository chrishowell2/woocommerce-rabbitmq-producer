<?php

namespace App\Models\PatronBase;

use Illuminate\Database\Eloquent\Model;

class ExtendedProperty extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblExtendedProperty';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    // protected $connection = 'sql';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}

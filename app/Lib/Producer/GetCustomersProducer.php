<?php

namespace App\Lib\Producer;

use App\Lib\PatronBase\Connections\PatronBaseConnection;
use App\Models\PatronBase\PatronEmail;
use Codexshaper\WooCommerce\Facades\Customer;

class GetCustomersProducer extends WoocommerceProducer
{
    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct(PatronBaseConnection $connection)
    {
        $this->connection = $connection;

        $this->routeKey = $connection->getNewPatronsRoutekey();
        $this->options = [
                'queue' => $connection->getNewPatronsQueueName(),
                'exchange' => $connection->getExchange()
        ];
        parent::__construct('NewPatron');
        $this->handle();
    }

    /**
     * Execute 
     *
     * @return void
     */
    public function handle()
    {
        $customer_payload = [];
        $email = '';

        try {
            $customers = Customer::where('date_created_gmt', '>=', $this->from)->get();
        } catch(\Exception $e) {
            return response()->json("There was a problem connecting to WooCommerce. Please check your API credentials");
        }

        foreach($customers as $customer) {
            $email = $customer->email;
            $is_patron = PatronEmail::on($this->connection->getDataBaseName())->where('Email', $email)
                                    ->first();

            if(empty($is_patron)) {
                array_push($customer_payload, $customer);
            }
        }

        $this->payload = $customer_payload;
    }

    public function getPayload()
    {
        return $this->payload;
    }
}

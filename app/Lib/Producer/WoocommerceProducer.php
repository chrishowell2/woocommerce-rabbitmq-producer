<?php
namespace App\Lib\Producer;

use App\Lib\PatronBase\ExtendedProperty;
use Bschmitt\Amqp\Facades\Amqp;
use Carbon\Carbon;

abstract class WoocommerceProducer
{
    protected $payload = null;

    protected $routeKey;

    protected $options;

     // From Date
     public $from;
     // To Date
     public $to;
     // Type key e.g NewPatron used for updating extended property
     public $type;
     // Extended property of patronbase
     public $extendedProperty;

    /*
    |--------------------------------------------------------------------------
    | Producer
    |--------------------------------------------------------------------------
    |
    | This Producer class provides a central location to place any logic that
    | is shared across all of your Producers.
    |
    */
    public function __construct($type)
    {
        $this->type = $type;
        $this->extendedProperty = new ExtendedProperty('LastWoocommerceSync', 'X', $this->connection);

        $this->dates = $this->extendedProperty->getValue();

        if (!$this->dates || empty($this->dates)) {
            $this->from = Carbon::now();

            $keys = [
                'NewPatron' => $this->from,
                'AmendedPatron' => $this->from,
                'NewSale' => $this->from,
                'NewSubscription' => $this->from
            ];
            $this->extendedProperty->updateValue($keys);
            $this->dates = $this->extendedProperty->getValue();
        }
        $this->from = $this->dates[$this->type];

        $this->to = Carbon::now();
    }

    public function send()
    {
        if (!empty($this->payload)) {

            $collection = collect($this->payload);
            $chunks = $collection->chunk(1);
            $data = $chunks->toArray();
            foreach($data as $payload) {
                Amqp::publish($this->routeKey, json_encode($payload), $this->options);
            }

            // Amqp::publish($this->routeKey, json_encode($this->payload), $this->options);

            $this->dates[$this->type] = $this->to;
            $this->extendedProperty->updateValue($this->dates);
        }
    }


}

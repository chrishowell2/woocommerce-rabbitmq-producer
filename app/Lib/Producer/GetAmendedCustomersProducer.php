<?php

namespace App\Lib\Producer;

use App\Lib\PatronBase\Connections\PatronBaseConnection;
use Codexshaper\WooCommerce\Facades\Customer;

class GetAmendedCustomersProducer extends WoocommerceProducer
{
    /**
     * Create a new customers instance.
     *
     * @return void
     */
    public function __construct(PatronBaseConnection $connection)
    {
        $this->connection = $connection;

        $this->routeKey = $connection->getAmendedPatronsRoutekey();
        $this->options = [
                'queue' => $connection->getAmendedPatronsQueueName(),
                'exchange' => $connection->getExchange()
        ];
        parent::__construct('AmendedPatron');
        $this->handle();
    }

    /**
     * Execute 
     *
     * @return void
     */
    public function handle()
    {
        $customer_payload = [];

        try {
            $customers = Customer::where('date_modified_gmt', '>=', $this->from)->get();
        } catch(\Exception $e) {
            return response()->json("There was a problem connecting to WooCommerce. Please check your API credentials");
        }

        foreach($customers as $customer) {
            array_push($customer_payload, $customer);
        }

        $this->payload = $customer_payload;
    }

    public function getPayload()
    {
        return $this->payload;
    }
}

<?php

namespace App\Lib\Producer;

use App\Lib\PatronBase\Connections\PatronBaseConnection;
use Carbon\Carbon;
use Codexshaper\WooCommerce\Facades\Order;

class NewSalesProducer extends WoocommerceProducer
{
    /**
     * Create a new instance.
     *
     * @return void
     */
    public function __construct(PatronBaseConnection $connection)
    {
        $this->connection = $connection;

        $this->routeKey = $connection->getNewSalesRoutekey();
        $this->options = [
                'queue' => $connection->getNewSalesQueueName(),
                'exchange' => $connection->getExchange()
        ];
        parent::__construct('NewSale');
        $this->handle();
    }

    /**
     * Execute the patron Stuff
     *
     * @return void
     */
    public function handle()
    {

        $date = strtotime($this->from);
        $formatted_date = date('Y-m-d\TH:i:s', $date);

        try {
            $orders = Order::where('date_created_gmt', '>=', $formatted_date)->get();
        } catch(\Exception $e) {
            return response()->json("There was a problem connecting to WooCommerce. Please check your API credentials");
        }

        $this->payload = $orders;
    }

    public function getPayload()
    {
        return $this->payload;
    }
}

<?php
namespace App\Lib\PatronBase\Connections;

abstract class PatronBaseConnection
{
    /*
    |--------------------------------------------------------------------------
    | Dispatcher
    |--------------------------------------------------------------------------
    |
    | This Dispatcher class provides a central location to place any logic that
    | is shared across all of your dispatchers.
    |
    */

    protected $exchange;
    protected $dataBaseName;


    /**
     *  Declared Classes for Subclasses
     *
     */

    abstract public function getNewPatronsRoutekey();
    abstract public function getNewPatronsQueueName();

    abstract public function getAmendedPatronsRoutekey();
    abstract public function getAmendedPatronsQueueName();

    abstract public function getNewSalesRouteKey();
    abstract public function getNewSalesQueueName();

    abstract public function getNewSubscriptionsRouteKey();
    abstract public function getNewSubscriptionsQueueName();


    /**
     * Returns connections database name
     *
     * @return string
     */
    public function getDataBaseName()
    {
        return $this->dataBaseName;
    }

    /**
     * Returns connections default exchange
     *
     * @return string
     */
    public function getExchange()
    {
        return $this->exchange;
    }

}

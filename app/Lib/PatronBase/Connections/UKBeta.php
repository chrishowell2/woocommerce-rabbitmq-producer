<?php

namespace App\Lib\PatronBase\Connections;

use App\Lib\PatronBase\Connections\PatronBaseConnection;

class UKBeta extends PatronBaseConnection
{
    /**
     * Create a new patronbase connection instance.
     *
     * This is like this as we may have to get things from a database or configurations?
     *
     * @return void
     */
    public function __construct()
    {
        $this->exchange = 'UKBeta';
        $this->dataBaseName = '_UKBeta';

    }

    /*
        Route keys and queues are a bit different so they must be the same for our method,
        but if you wanted to say for example send it to multiple queues you can use a route key
    */

    /**
     *  New Patrons
     */
    public function getNewPatronsRouteKey()
    {
        return $this->exchange.'-NewPatrons';
    }

    public function getNewPatronsQueueName()
    {
        return $this->exchange.'-NewPatrons';
    }

    /**
     *  Amend Patrons
     */
    public function getAmendedPatronsRouteKey()
    {
        return $this->exchange.'-AmendedPatrons';
    }

    public function getAmendedPatronsQueueName()
    {
        return $this->exchange.'-AmendedPatrons';
    }

    /**
     *  New Sale
     */
    public function getNewSalesRouteKey()
    {
        return $this->exchange.'-NewSales';
    }

    public function getNewSalesQueueName()
    {
        return $this->exchange.'-NewSales';
    }

     /**
     *  New Subscription
     */
    public function getNewSubscriptionsRouteKey()
    {
        return $this->exchange.'-NewSubscriptions';
    }

    public function getNewSubscriptionsQueueName()
    {
        return $this->exchange.'-NewSubscriptions';
    }

}

<?php

namespace App\Lib\PatronBase;

use App\Models\PatronBase\ExtendedProperty as ExtendedPropertyModel;

class ExtendedProperty
{
    /**
     * A map of value types and their associated columns.
     *
     * @var array
     */
    protected static $VALUE_COLUMNS = [
        'S' => 'TextValue',
        'D' => 'DateValue',
        'I' => 'IntValue',
        'F' => 'FloatValue',
        'X' => 'XmlValue',
    ];

    /**
     * The type of application using the extended property.
     *
     * 'E' = Extension
     *
     * @var string
     */
    protected static $KEY_TYPE = 'E';

    /**
     * The name of the application using the extended property.
     *
     * @var string
     */
    protected static $PROPERTY_KEY = 'WoocommerceIntegration';

    /**
     * The name of the extended property.
     *
     * @var string
     */
    protected $propertyName;

    /**
     * The value type to be stored in the extended property.
     *
     * 'S' = String
     * 'D' = Date
     * 'I' = Integer
     * 'F' = Float
     * 'X' = Xml
     *  Example
     *  $property = new ExtendedProperty('LastImportDate', 'D');
     *  $from = $property->getValue();
     *  $updated = $property->updateValue($to);
     *
     * @var [type]
     */
    protected $valueType;

    public function __construct($propertyName = null, $valueType = null, $connection = null)
    {
        $this->propertyName = $propertyName;
        $this->valueType = $valueType;
        $this->connection = $connection ? $connection->getDataBaseName() : 'sqlsrv';
    }

    /**
     * Update the value of the extended property.
     *
     * @param mixed $value
     * @return \App\Models\PatronBase\ExtendedProperty
     */
    public function updateValue($value)
    {
        $this->firstOrCreate();

        if ($this->valueType == 'X') {
            $value = $this->convertValueToXml($value);
        }

        $property = ExtendedPropertyModel::on($this->connection)
            ->where('PropertyName', $this->propertyName)
            ->where('PropertyKey', self::$PROPERTY_KEY)
            ->update([$this->getValueColumn() => $value]);

        return $property;
    }

    /**
     * Get the value of the extended property.
     *
     * @return mixed
     */
    public function getValue()
    {
        $value = ExtendedPropertyModel::on($this->connection)
            ->where('PropertyName', $this->propertyName)
            ->where('PropertyKey', self::$PROPERTY_KEY)
            ->pluck($this->getValueColumn())
            ->first();

        if ($value && $this->valueType == 'X') {
            return $this->parseXmlValue($value);
        }

        return $value;
    }

    /**
     * Return an XML value as an associative array.
     *
     * @param string $value
     * @return array
     */
    protected function parseXmlValue($value)
    {
        $object = new \SimpleXMLElement($value);

        return json_decode(json_encode($object), true);
    }

    /**
     * Convert an array value to XML and remove the XML header.
     *
     * @param array $value
     * @return string
     */
    protected function convertValueToXml($value)
    {
        $xml = $this->arrayToXml($value);

        return substr($xml, strpos($xml, '?>') + 2);
    }

    /**
     * Convert an array to XML.
     *
     * @param array $value
     * @param mixed $element
     * @param \SimpleXMLElement $xml
     * @return string
     */
    protected function arrayToXml($value, $element = null, $xml = null)
    {
        if ($xml == null) {
            $xml = new \SimpleXMLElement($element ?? '<root/>');
        }

        foreach ($value as $key => $item) {
            if (is_numeric($key)) {
                $key = 'Row';
            }

            if (is_array($item)) {
                $this->arrayToXml($item, $key, $xml->addChild($key));
            } else {
                $xml->addChild($key, $item);
            }
        }

        return $xml->asXML();
    }

    /**
     * Insert the property if it doesn't already exist,
     *
     * @return \App\Models\PatronBase\ExtendedProperty
     */
    protected function firstOrCreate()
    {
        return ExtendedPropertyModel::on($this->connection)
            ->firstOrCreate([
                'PropertyName' => $this->propertyName,
                'PropertyKey' => self::$PROPERTY_KEY,
                'GroupName' => '',
                'KeyType' => self::$KEY_TYPE,
                'ValueType' => $this->valueType,
            ]);
    }

    protected function getValueColumn()
    {
        return self::$VALUE_COLUMNS[$this->valueType];
    }

    public function appendXmlArrayValue($value)
    {
        $values = $this->getValue();
        if (!$values) {
            $values = [];
            array_push($values, $value);
            $value = $this->convertValueToXml($values);
            ExtendedPropertyModel::on($this->connection)
            ->Create([
                'PropertyName' => $this->propertyName,
                'PropertyKey' => self::$PROPERTY_KEY,
                'GroupName' => '',
                'KeyType' => self::$KEY_TYPE,
                'ValueType' => $this->valueType,
                'XmlValue' => $value
            ]);
            return $this->getValue();
        }
        array_push($values['Row'], $value);

        $values = $this->convertValueToXml($values['Row']);
        $model = ExtendedPropertyModel::on($this->connection)
            ->where('PropertyName', $this->propertyName)
            ->where('PropertyKey', self::$PROPERTY_KEY)
            ->update([$this->getValueColumn() => $values]);


        return $this->getValue();
    }
}

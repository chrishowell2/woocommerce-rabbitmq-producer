#!/usr/bin/env bash
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list


apt-get update

#essentials
apt-get install software-properties-common git vim curl language-pack-en -y

#get latest php repo
add-apt-repository -y ppa:ondrej/php
add-apt-repository -y ppa:chris-lea/redis-server
apt-get update

#apache and mod rewrite module
apt-get install -y apache2
a2enmod rewrite

#php
apt-get install -y php7.3-dev php7.3 libapache2-mod-php7.3 php7.3-curl php7.3-gd php7.3-mbstring php7.3-dom php7.3-zip php7.3-xml php-amqp
apt-get install -y openssl

apt-get install -y php-redis


ACCEPT_EULA=Y apt-get install -y msodbcsql mssql-tools unixodbc-dev
sudo pecl install sqlsrv

wget http://pecl.php.net/get/pdo_sqlsrv-5.3.0.tgz
pear install pdo_sqlsrv-5.3.0.tgz

echo extension=pdo_sqlsrv.so >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/30-pdo_sqlsrv.ini
echo extension=sqlsrv.so >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/20-sqlsrv.ini
echo "extension=pdo_sqlsrv.so" >> /etc/php/7.3/apache2/conf.d/30-pdo_sqlsrv.ini
echo "extension=sqlsrv.so" >> /etc/php/7.3/apache2/conf.d/20-sqlsrv.ini

If you have any problems with the above commands copy the pdo_sqlsrv.so and sqlsvr.so to /usr/lib/php/20180731
cp /var/www/producer/vagrant/php7.3/sqlsrv.so /usr/lib/php/20180731/sqlsrv.so
cp /var/www/producer/vagrant/php7.3/pdo_sqlsrv.so /usr/lib/php/20180731/pdo_sqlsrv.so

#apt-get install -y freetds-common
#apt-get install -y freetds-bin
#apt-get install -y php7.3-sybase

#cp /vagrant/vagrant/freetds.conf /etc/freetds/freetds.conf

#redis
apt-get install -y redis-server

#postgres
# apt-get install -y php7.3-pgsql
# apt-get install -y postgresql postgresql-contrib
# useradd app
# sudo chpasswd << 'END'
# app:app
# END
# echo "CREATE USER app WITH PASSWORD 'app';" | sudo -u postgres psql
# echo "CREATE DATABASE app;" | sudo -u postgres psql
# echo "GRANT ALL PRIVILEGES ON DATABASE app to app;" | sudo -u postgres psql

#mysql
#sudo apt-key adv --keyserver pgp.mit.edu --recv-keys A4A9406876FCBD3C456770C88C718D3B5072E1F5
#if the keys are out of date for my sql run the above
# export DEBIAN_FRONTEND=noninteractive
# echo mysql-apt-config mysql-apt-config/select-server select mysql-5.7 | debconf-set-selections
# echo mysql-apt-config mysql-apt-config/repo-codename select xenial | debconf-set-selections
# echo mysql-apt-config mysql-apt-config/repo-distro select ubuntu | debconf-set-selections
# echo mysql-apt-config mysql-apt-config/select-product select Apply | debconf-set-selections
# curl -O http://repo.mysql.com/mysql-apt-config_0.7.3-1_all.deb && dpkg -i mysql-apt-config_0.7.3-1_all.deb
# apt-get update
# apt-get install -y php7.3-mysql
# echo mysql-community-server mysql-community-server/root-pass password PASSWORD | debconf-set-selections
# echo mysql-community-server mysql-community-server/re-root-pass password PASSWORD | debconf-set-selections
# apt-get install -y --allow-unauthenticated mysql-community-server

#mysql database user set up
# echo "create database payment" | mysql -u root -pPASSWORD
# echo "CREATE USER 'payment'@'%' IDENTIFIED BY 'payment'"  | mysql -u root -pPASSWORD
# echo "GRANT ALL ON *.* to 'payment'@'%'" | mysql -u root -pPASSWORD
# echo "CREATE USER 'payment'@'localhost' IDENTIFIED BY 'payment'"  | mysql -u root -pPASSWORD
# echo "GRANT ALL ON *.* to 'payment'@'localhost'" | mysql -u root -pPASSWORD



#link site file in vagrant to defualt directory
ln -fs /vagrant /var/www/producer

#virtual hosts
rm /etc/apache2/sites-enabled/*
cp /vagrant/vagrant/virtualhost.conf /etc/apache2/sites-enabled/virtualhost.conf

service apache2 restart


#clean clears out the local repository of retrieved package files.
apt-get clean

#composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#.env
cp /vagrant/vagrant/.env-vagrant /vagrant/.env

cd /vagrant && composer install

#php artisan migrate --seed
#redis-server --daemonize yes

<?php

return [
    'connections' => [
        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'host' => env('SQL_HOST'),
            'database' => env('SQL_DATABASE'),
            'username' => env('SQL_USERNAME'),
            'password' => env('SQL_PASSWORD'),
            'port' => env('SQL_PORT'),
            'charset' => 'utf8',
        ]
    ]
];
